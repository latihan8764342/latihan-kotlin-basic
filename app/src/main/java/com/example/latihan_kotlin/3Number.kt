package com.example.latihan_kotlin

fun main() {
    // INTEGER NUMBER
    var age = 200 // var age: Int = 200
    println(age)

    //FLOATING NUMBER
    var sample = 10.0f//var sample : Float = 10.0f
    println(sample)

    //LITERALS
    var binary: Int = 0b1101101
    println(binary)

    //UNDERSCORE
    var price: Long = 9_000_000_000L
    println(price)

    //CONVERSION
    var priceInt: Int = price.toInt()
    println(priceInt)

    var doubleBinary: Double = binary.toDouble()
    println(doubleBinary)
}

