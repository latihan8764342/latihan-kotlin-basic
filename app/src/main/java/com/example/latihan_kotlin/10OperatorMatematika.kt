package com.example.latihan_kotlin

fun main() {
    val result = 10 / 6
    val resul = 10.0 / 4.0
    println(resul)
    println(result)

    val result2 = 10 + 10 / 2
    println(result2)

    var total: Int = 0
    val barang1 = 100
    total += barang1

    val barang2 = 200
    total += barang2

    val barang3 = 300
    total = total + barang3

    println(total)

    total++ //total = total + 1
    total++
    total++
    println(total)

    val suhu = -5
    println(suhu)

    val sehat = true
    println(!sehat)

}