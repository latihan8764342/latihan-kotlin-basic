package com.example.latihan_kotlin
//
//class makanan (val nama: String, val harga: Double)
//
//fun main(){
//    val menu = listOf(
//        makanan("nasi goreng", 25000.0),
//        makanan("ayam bakar", 20000.0),
//        makanan("sate ayam", 19000.0),
//        makanan("bubur ayam", 15000.0)
//    )
//
//    println("Menu Makanan")
//}


//fun main() {
//    println("Masukkan sebuah bilangan: ")
//    val input = readLine()
//
//    val bilangan = input?.toIntOrNull()
//
//    if (bilangan != null) {
//        if (bilangan % 2 == 0) {
//            println("$bilangan adalah bilangan genap")
//        } else {
//            println("$bilangan adalah bilangan ganjil")
//        }
//    } else {
//        println("Input bukan bilangan bulat")
//    }
//}

class Makanan(val nama: String, val harga: Int)

class Pengiriman(val metode: String)

class Order {
    private val items = mutableListOf<Makanan>()

    fun addItem(makanan: Makanan) {
        items.add(makanan)
    }

    fun hargaTotal(): Int {
        var total = 0
        for (item in items) {
            total = total + item.harga
        }
        return total
    }

    fun displayOrder() {
        println("Pesanan Anda:")
        for ((index, item) in items.withIndex()) {
            println("${index + 1}. ${item.nama} = Rp ${item.harga}")
        }
        println("Total: Rp ${hargaTotal()}")
    }

}


fun main() {
    val menu: List<Makanan> = listOf(
        Makanan("Nasi Goreng", 15000),
        Makanan("Ayam Bakar", 20000),
        Makanan("Mie Goreng", 12000),
        Makanan("Sate Ayam", 15000)
    )

    println("Menu Makanan:")
    for ((index, makanan) in menu.withIndex()) {
        println("${index + 1}. ${makanan.nama} = Rp ${makanan.harga}/porsi")
    }

    val order = Order()
    var continueOrdering = true

    while (continueOrdering) {
        println("Pilih nomor makanan yang ingin dipesan (0 untuk selesai): ")
        val pilhan = readLine()?.toIntOrNull() ?: 0

        if (pilhan in 1..menu.size) {
            order.addItem(menu[pilhan - 1])
            println("${menu[pilhan - 1].nama} telah ditambahkan ke dalam pesanan.")
        } else if (pilhan == 0) {
            continueOrdering = false
        } else {
            println("Pilihan tidak valid.")
        }
    }

    order.displayOrder()

    println("Masukkan Pembayaran: ")
    val pembayaran: Int = readLine()?.toIntOrNull() ?: 0
    when{
        pembayaran >= order.hargaTotal() -> println("Pembayaran Berhasil")
        pembayaran < order.hargaTotal() -> println("Pembayaran Gagal")
    }

    val metodePengiriman :List<Pengiriman> = listOf(
        Pengiriman("Take Away"),
        Pengiriman("Delivery")
    )

    println("Metode Pengiriman")
    for ((index: Int, metode: Pengiriman) in metodePengiriman.withIndex()){
        println("${index + 1}. ${metode.metode} ")
    }


    val pilihMetodePengiriman = true

    while (pilihMetodePengiriman){
        println("Pilih Metode Pengiriman:")
        val pilihPengiriman = readLine()?.toIntOrNull() ?: 0

        if (pilihPengiriman == 1){
            println("Makananmu sedang dimasak.....")
            println("Makanan siap, silahkan ambil di resto")
            println("Pesanan selesai")
        }
        else if (pilihPengiriman == 2){
            println("Makananmu sedang dimasak.....")
            println("Makanan siap, Driver menuju resto")
            println("Driver sampai, Pesanan selesai")
        }
        break
    }
}


