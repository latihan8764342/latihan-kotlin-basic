package com.example.latihan_kotlin

fun main() {
    var firstName : String = "Muhammad Fauzi"
    var lastName : String = "Rahmanuddin"

    var addres : String = """
        Jalan H SOLEH II
        Kecamatan Kebon Jeruk
        Jakarta Barat
    """.trimIndent()

    println(firstName)
    println(lastName)
    println(addres)

    var fullname : String = "$firstName $lastName" //var fullname : String = firstName + " " + lastName
    println(fullname)

    var desc : String = "total karakter $fullname = ${fullname.length}"
    println(desc)
}