package com.example.latihan_kotlin

//class Manusia(
//    val nama: String,
//    val kelamin: String,
//    val tinggiBadan:Int,
//    val beratBadan: Int,
//)   {
//}
//
//fun main() {
//    val mahasiswa = Manusia(
//        nama = "Fauzi",
//        kelamin = "Pria",
//        tinggiBadan = 175,
//        beratBadan = 120,
//    )
//    println("Nama: ${mahasiswa.nama}")
//    println("kelamin: ${mahasiswa.kelamin}")
//    println("Tinggi Badan: ${mahasiswa.tinggiBadan}")
//    println("Berat Badan: ${mahasiswa.beratBadan}")
//}
fun main() {
    val zara = Manusia("Fauzi", 190)
}
class Manusia {
    var nama: String
    var tinggiBadan: Int

    init {
        println("Initializer Block")
    }

constructor(_nama:String, _tinggi:Int){
    this.nama= _nama
    this.tinggiBadan= _tinggi
    println("Nama = $nama")
    println("Tinggi Badan = $tinggiBadan")
}
}





/*    import java.text.NumberFormat
            import java.util.Locale

   fun main() {
        val amount: Int = 50000

        // Format sebagai mata uang
        val currencyFormat = NumberFormat.getCurrencyInstance(Locale("id", "ID"))
        val formattedCurrency = currencyFormat.format(amount)

        // Menghapus nol yang berlebihan dari format mata uang
        val trimmedFormattedCurrency = formattedCurrency.replace(",00", "")
        println("Formatted Currency (Trimmed): $trimmedFormattedCurrency")

        // Format sebagai angka dengan pemisah ribuan
        val numberFormat = NumberFormat.getNumberInstance(Locale("id", "ID"))
        val formattedNumber = numberFormat.format(amount)

        // Menghapus nol yang berlebihan dari format angka
        val trimmedFormattedNumber = formattedNumber.replace(",00", "")
        println("Formatted Number (Trimmed): $trimmedFormattedNumber")
    } */

/*class Food(val name: String, val price: Double)

class Order {
    private val items = mutableListOf<Food>()

    fun addItem(food: Food) {
        items.add(food)
    }

    fun getTotal(): Double {
        var total = 0.0
        for (item in items) {
            total += item.price
        }
        return total
    }

    fun displayOrder() {
        println("Pesanan Anda:")
        for ((index, item) in items.withIndex()) {
            println("${index + 1}. ${item.name} - Rp ${item.price}")
        }
        println("Total: Rp ${getTotal()}")
    }
}

fun main() {
    val menu = listOf(
        Food("Nasi Goreng", 15000.0),
        Food("Ayam Bakar", 20000.0),
        Food("Mie Goreng", 12000.0),
        Food("Sate Ayam", 15000.0)
    )

    println("Selamat datang di Aplikasi Pemesanan Makanan!")

    val order = Order()
    var continueOrdering = true

    while (continueOrdering) {
        println("Menu Makanan:")
        for ((index, food) in menu.withIndex()) {
            println("${index + 1}. ${food.name} - Rp ${food.price}")
        }

        println("Pilih nomor makanan yang ingin dipesan (0 untuk selesai): ")
        val choice = readLine()?.toIntOrNull() ?: 0

        if (choice in 1..menu.size) {
            order.addItem(menu[choice - 1])
            println("${menu[choice - 1].name} telah ditambahkan ke dalam pesanan.")
        } else if (choice == 0) {
            continueOrdering = false
        } else {
            println("Pilihan tidak valid.")
        }
    }

    order.displayOrder()

    println("Total yang harus dibayar: Rp ${order.getTotal()}")
    println("Apakah Anda ingin melanjutkan pembayaran? (y/n)")
    val confirmation = readLine()

    if (confirmation?.toLowerCase() == "y") {
        println("Pembayaran berhasil! Terima kasih telah memesan makanan.")
    } else {
        println("Pesanan dibatalkan. Terima kasih atas kunjungannya.")
    }
}
*/