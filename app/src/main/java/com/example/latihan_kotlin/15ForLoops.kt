package com.example.latihan_kotlin

fun main() {
    // FOR ARRAY

    var array =  arrayOf("Muhammad", "Fauzi", "Rahmanuddin")

    var total = 0
    for (name in array) {
        println(name)
        total++
    }
    println("total perulangan = $total")

    //FOR RANGE
    for (i in 100 downTo 0  step 25){
        println(i)
    }

    val ukuranArray = array.size - 1
    for (i in 0..ukuranArray){
        println("Imdex ke- $i = ${array.get(i)}")
    }
}