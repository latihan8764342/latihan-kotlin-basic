package com.example.latihan_kotlin

// variable constant
const val APP = "BELAJAR KONTLIN"
const val VERSION = "0.0.1"

fun main() {

    println("$APP : $VERSION")

    var firstName = "Fauzi"
    firstName = "Muhammad Fauzi"
    val lastName = "Rahmanuddin"
    val age = 20

    println("$firstName $lastName")
    println(age)

    // Nullable
    var namaDepan : String? = null
    //  namaDepan = "fauzi"
    println(namaDepan)
    println(namaDepan?.length)

}