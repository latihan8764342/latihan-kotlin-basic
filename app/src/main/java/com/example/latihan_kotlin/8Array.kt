package com.example.latihan_kotlin

fun main() {
    val numbers: Array<Int> = arrayOf(2, 3, 4)
    numbers.set(0, 9)
//    numbers[0] = 9
    println(numbers[0])

    val nama: Array<String> = arrayOf("fauzi", "joko", "mara")
    println(nama[0])
    println(nama[1])
    println(nama[2])

    //Array Nullable
    val angka: Array<String?> = arrayOfNulls(5)
    angka[0] = "satu"
    angka[1] = "satu"
    angka[2] = "satu"
    angka[3] = null
    angka[4] = "satu"
    //  angka[5] = "satu"
    angka.set(1, "dua")

    println(angka[1])
    println(angka.get(3))
    println(angka.size)
}