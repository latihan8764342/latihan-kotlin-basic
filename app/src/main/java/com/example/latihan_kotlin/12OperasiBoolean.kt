package com.example.latihan_kotlin

fun main() {

    val nilaiUjian = 80
    val nilaiAbsen = 60
    val nilaiExtra = 80

    val apakahLulusUjian = nilaiUjian > 75
    val apakahLulusAbsen = nilaiAbsen > 75
    val apakaHlulusExtra = nilaiExtra > 75

    val apakahLulus = apakahLulusUjian && apakahLulusAbsen && apakaHlulusExtra
    println(apakahLulus)

}