package com.example.latihan_kotlin

fun main() {
    val nilai = 70

    if (nilai >= 80) {
        println("Cool")
    } else if (nilai >= 70) {
        println("Good")
    } else if (nilai >= 60){
        println("Not Bad")
    } else {
        println("Tidak Lulus")
    }
}