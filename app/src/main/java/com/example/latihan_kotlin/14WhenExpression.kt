package com.example.latihan_kotlin

fun main() {

    var nilai = "A"

    when (nilai) {
        "A" -> println("Amazing")
        "B" -> println("Good")
        "C" -> println("Not Bad")
        "D" -> println("Bad")
        else -> println("Tidak Lulus")
    }

    val tipeData: Any = "hot"

    when(tipeData){
        is Float -> println("Float")
        is String -> println("String")
        else -> println("Else")
    }

    when (nilai) {
        "A", "B", "C" -> println("Selamat Anda Lulus")
        else -> println("Anda Tidak Lulus")
    }

    val nilaiLulus = arrayOf("A", "B", "C")
    when (nilai){
        in nilaiLulus -> println("ANDA LULUS")
        !in nilaiLulus-> println("ANDA GAGAL")
    }

    val name = "Fauzi"
    when(name){
        is String -> println("Mame is String")
        !is String -> println("Namr is not String")
    }

    val nilaiUjian = 60
    when{
        nilaiUjian > 90 -> println("Good Job")
        nilaiUjian > 80 -> println("oke")
        nilaiUjian > 70 -> println("Not Bad")
        else -> println("Anda Gagal")
    }

}
